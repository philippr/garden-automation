#include <TinyWireS.h>


#define SLAVE_ADDR 9 // temperature sensor
#define adc_pin 2 // PIN 4, analog 2
#define i2c_sda 0
#define i2c_sck 2


void setup() {
  pinMode(adc_pin, INPUT); // Analog Input
 // analogReference(INTERNAL2V56);  doesn't work
  TinyWireS.begin(SLAVE_ADDR);
  TinyWireS.onRequest(answer_master);
}

int read_sensor() {
  int value = analogRead(adc_pin);
 return value;
}

void answer_master() {
    TinyWireS.send(read_sensor());
}
void loop() {
  delay(1);
}
