#include <Wire.h>
#define i2c_slave_light 8
#define i2c_slave_temp 9
#define i2c_slave_moist 10

void setup() {
  Wire.begin();        // join i2c bus (address optional for master)
  Serial.begin(9600);  // start serial for output
  Serial.println("Start....");
}




void loop() {
  Wire.requestFrom(i2c_slave_light, 1);    // request 1 bytes from slave device #1
  int light_val = Wire.read();
  Serial.print("Light-Sensor:"); 
  Serial.println(light_val);

  Wire.requestFrom(i2c_slave_temp, 1); 
  int temp_val = Wire.read();
  Serial.print("Temp-Sensor:"); 
  Serial.println(temp_val);

  
  Wire.requestFrom(i2c_slave_moist, 1); 
  int moist_val = Wire.read();
  Serial.print("Moist-Sensor:"); 
  Serial.println(moist_val);

  /*
  while (Wire.available()) { // slave may send less than requested
    int c = Wire.read(); // receive a byte as character
    Serial.print("Light-Sensor:"); 
    Serial.println(c);
  }

*/
  delay(500);
}
