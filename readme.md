# Garden Automation

## Next steps 

- wireless slave communication (rf-448mhz)
- attiny deep sleep (watchdog?)
- Rewrite software in c for bare attiny85
- smd size, power supply
- relay connectivity (gsm)
- Outdoor sustainability


## Sensors

### Progress 

| Sensor | Prototype | Calibration | Final |
| --- | --- | --- | ---- |
| Light	    |	x   |	    |	    |
| Temperature | x   |	    |	    |
| Humidity  |	    |	    |	    |
| Moisture  |	x   |	    |	    |
| Water-Level |	   |	    |	    |

### General Pin-Map

| Pin | Function |
|---  | ---	 |
| 0   | SDA	 |
| 1   |		 |
| 2   | SCL	 |
| 3   |		 | 
| 4   | ADC	 |
| 5   |		 | 

### Light

### Temperature

### Moisture
TODO:
- PIN5 sensor enable 

### Water-Level (Ultra-Sonic)

## Libraries and Dependencies

Digispark Attiny85 Development Boards:
- [board libray](https://digistump.com/wiki/digispark/tutorials/connectingpro)
- [examples](https://digistump.com/wiki/digispark/tutorials/basics#digispark_basics)
- [pinout](https://digistump.com/wiki/digispark/tutorials/pinguide)


dialout udev:

```bash
$ cat /etc/udev/rules.d/digispark.rules 
SUBSYSTEM=="usb", ATTR{idVendor}=="16d0", ATTR{idProduct}=="0753", MODE="0660", GROUP="dialout"
```

I2C
- [tinyWire](https://github.com/rambo/TinyWire)

Arduino Nano:
- programming error (stk500 timeout blup): select old bootloader

